package com.lowcode.infrastructure.support;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.lowcode.model.User;

public abstract class HibernateDAOSupport extends UserGroupSupport{

	private static Logger logger = LoggerFactory.getLogger(HibernateDAOSupport.class);

	public abstract SessionFactory getSessionFactory();

	public Session getSession() {
		Session currentSession = null;
		try {
			currentSession = getSessionFactory().getCurrentSession();
		} catch (Exception e) {
			logger.error("Error while getting current session!", e);
		}
		return currentSession;
	}

	@Transactional
	public void save(Object model) {
		getSession().save(model);
	}

	@Transactional
	public void saveOrUpdate(Object model) {
		getSession().saveOrUpdate(model);

	}

	@Transactional
	public void update(Object model) {
		getSession().merge(model);
	}

	public Object saveOrUpdateObject(Object model) {
		getSession().saveOrUpdate(model);
		return model;
	}

	public void delete(Object model) {
		getSession().delete(model);
	}

	@SuppressWarnings("unchecked")
	public static <T> List<T> criteria(Criteria criteria) {
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public <T> T getById(Class<?> clazz, Long id) {
		return (T) getSession().createCriteria(clazz).add(Restrictions.eq("id", id)).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public <T> T getByField(Class<?> clazz, String field, Object id) {
		if (clazz.equals(User.class)) {
			return (T) getSession().createCriteria(clazz).setFetchMode("provider", FetchMode.JOIN)
					.add(Restrictions.eq(field, id)).uniqueResult();
		}
		return (T) getSession().createCriteria(clazz).add(Restrictions.eq(field, id)).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getListOfRecordsByField(Class<?> clazz, String field, Object id) {
		return getSession().createCriteria(clazz).add(Restrictions.eq(field, id)).list();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getAll(Class<?> clazz) {
		return getSession().createCriteria(clazz).list();
	}
}
