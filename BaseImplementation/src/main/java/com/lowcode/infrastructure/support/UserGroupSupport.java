package com.lowcode.infrastructure.support;


import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import com.lowcode.common.error.ErrorCodes;
import com.lowcode.common.error.ErrorId;
import com.lowcode.common.error.ErrorId.Severity;
import com.lowcode.exceptions.LowCodeUnauthorizedException;


public abstract class UserGroupSupport {

	public String getLoggedInUserName() throws LowCodeUnauthorizedException {

		if (SecurityContextHolder.getContext() != null &&  (SecurityContextHolder.getContext().getAuthentication() != null)) {
				Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				if (obj instanceof User) {
					User user = (User) obj;
					return user.getUsername();
				}
			
		}

		throw new LowCodeUnauthorizedException(new ErrorId(ErrorCodes.INVALID_USER.getErrorCode(),
				ErrorCodes.INVALID_USER.getErrorMessage(), Severity.FATAL), HttpStatus.UNAUTHORIZED);
	}

	

}
