package com.lowcode.infrastructure.config;

import java.beans.PropertyVetoException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import com.lowcode.sqlconnection.property.DataBaseProperties;
import com.mchange.v2.c3p0.ComboPooledDataSource;



@Configuration
@EnableTransactionManagement
public class DatasourceConfig {

	@Autowired
	private DataBaseProperties databaseProperties;

	@Autowired
	@Bean(name = "dataSource")
	public DataSource getMySqlDataSource() {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		try {
			dataSource.setDriverClass(databaseProperties.getJdbcDriver());
			dataSource.setJdbcUrl(databaseProperties.getJdbcUrl());
			dataSource.setUser(databaseProperties.getJdbcUser());
			dataSource.setPassword(databaseProperties.getJdbcPassword());

			if (databaseProperties.getMasterPreferredTestQuery() != null
					&& !databaseProperties.getMasterPreferredTestQuery().isEmpty()) {
				dataSource.setPreferredTestQuery(databaseProperties.getMasterPreferredTestQuery());
			}

			if (databaseProperties.getMysqlMinPoolSize() != null
					&& !databaseProperties.getMysqlMinPoolSize().isEmpty()) {

				dataSource.setMinPoolSize(Integer.parseInt(databaseProperties.getMysqlMinPoolSize()));
			}

			if (databaseProperties.getMysqlMaxPoolSize() != null
					&& !databaseProperties.getMysqlMaxPoolSize().isEmpty()) {

				dataSource.setMaxPoolSize(Integer.parseInt(databaseProperties.getMysqlMaxPoolSize()));
			}

			if (databaseProperties.getMysqlIdleConnectionTestPeriod() != null
					&& !databaseProperties.getMysqlIdleConnectionTestPeriod().isEmpty()) {

				dataSource.setIdleConnectionTestPeriod(
						Integer.parseInt(databaseProperties.getMysqlIdleConnectionTestPeriod()));
			}

			if (databaseProperties.getMysqlMaxIdleTimeOut() != null
					&& !databaseProperties.getMysqlMaxIdleTimeOut().isEmpty()) {

				dataSource.setMaxIdleTime(Integer.parseInt(databaseProperties.getMysqlMaxIdleTimeOut()));
			}

			if (databaseProperties.getMysqlMaxIdleTimeExcessConnections() != null
					&& !databaseProperties.getMysqlMaxIdleTimeExcessConnections().isEmpty()) {

				dataSource.setMaxIdleTimeExcessConnections(
						Integer.parseInt(databaseProperties.getMysqlMaxIdleTimeExcessConnections()));
			}
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
		return dataSource;
	}


	@Bean(name = "datasource.activiti")
	public DataSource activitiDataSource() {
		return DataSourceBuilder.create().url(databaseProperties.getJdbcUrl())
				.username(databaseProperties.getJdbcUser()).password(databaseProperties.getJdbcPassword())
				.driverClassName(databaseProperties.getJdbcDriver()).build();
	}

	@Autowired
	@Bean(name = "userdataSource")
	public DataSource getUserDataSource() {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		try {
			dataSource.setDriverClass(databaseProperties.getJdbcDriver());
			dataSource.setJdbcUrl(databaseProperties.getJdbcUrl());
			dataSource.setUser(databaseProperties.getJdbcUser());
			dataSource.setPassword(databaseProperties.getJdbcPassword());

			if (databaseProperties.getMysqlMinPoolSize() != null
					&& !databaseProperties.getMysqlMinPoolSize().isEmpty()) {
				dataSource.setMinPoolSize(Integer.parseInt(databaseProperties.getMysqlMinPoolSize()));
			}

			if (databaseProperties.getMysqlMaxPoolSize() != null
					&& !databaseProperties.getMysqlMaxPoolSize().isEmpty()) {

				dataSource.setMaxPoolSize(Integer.parseInt(databaseProperties.getMysqlMaxPoolSize()));
			}

		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
		return dataSource;
	}

}
