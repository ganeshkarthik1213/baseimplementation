package com.lowcode.infrastructure.config;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.lowcode.infrastructure.property.HibernateProperty;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan("com.lowcode")
@EnableTransactionManagement
@EnableAutoConfiguration
@EnableAsync
public class ApplicationContextConfig implements WebMvcConfigurer {

	@Autowired
	private HibernateProperty hibernateProperty;

	private static final Logger logger = LoggerFactory.getLogger(ApplicationContextConfig.class);

	@Autowired
	DataSource dataSource;

	@Autowired
	@Bean(name = "entityManagerFactory")
	public SessionFactory getSessionFactory() {
		LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
		 sessionBuilder.addProperties(hibernateProperty.getHibernateProperties());
		sessionBuilder.scanPackages("com.lowcode.model");
		logger.info("Created SessionFactory!");
		return sessionBuilder.buildSessionFactory();
	}

	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
		logger.info("Created Transaction Manager");
		return transactionManager;
	}

	@Autowired
	@Bean(name = "hibernateTemplate")
	public HibernateTemplate hibernateTemplate(SessionFactory sessionFactory) {
		logger.info("Created Hibernate Template!");
		return new HibernateTemplate(sessionFactory);
	}

}
