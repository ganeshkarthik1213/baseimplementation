package com.lowcode.CommonDaoImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.lowcode.model.DataFilterMainVo;
import com.lowcode.model.SubGroupVo;
import com.lowcode.exceptions.LowCodeException;
import com.lowcode.model.User;
import com.lowcode.model.MongoFilterVo;
import com.lowcode.CommonEntity.Page1;

public interface Page1DaoImpl {

	void deleteRecord(String collectionName, String id);

	void saveRecord(Page1 entity);

	void updateRecord(String id, HashMap<String, Object> entity);

	//List<Map<String, Object>> getAllRecord(Integer limit, Integer skip);

	//List<Map<String, Object>> getSingleRecord(String id);

	//Object getDataById(String id);
	
	//List<Map<String, Object>> getFilterData(List<SubGroupVo> subGroupFilter, Integer limit, Integer skip, User loggedinUser)  throws LowCodeException;

	//Integer getFilterDataCount(List<SubGroupVo> subGroupFilter, User loggedinUser) throws LowCodeException;

	//Integer getAllRecordsCount();

	//List<Map<String, Object>> getSpecialEntityRecord(MongoFilterVo vo, User loggedinUser, List<Integer> usedId, DataFilterMainVo dataFilter, Boolean isSpecilaEntity)  throws LowCodeException;

}
