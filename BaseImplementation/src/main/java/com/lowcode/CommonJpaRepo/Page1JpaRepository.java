package com.lowcode.CommonJpaRepo;

import com.lowcode.CommonEntity.Page1;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

@Repository
public interface Page1JpaRepository extends JpaRepository<Page1, Integer> {
  
      @Query(value = "SELECT * FROM Page1 WHERE _id = ?1", nativeQuery = true)
      Page1 getDataById(String id);
}