package com.lowcode.sqlconnection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({ "com.lowcode" })
@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
public class MySqlConnectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(MySqlConnectionApplication.class, args);
	}

}
