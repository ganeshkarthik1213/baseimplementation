package com.lowcode.sqlconnection.property;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:/database.properties")
public class DataBaseProperties {

	@Autowired
	private Environment databaseProperties;

	@Autowired
	private EnvironmentProperties environmentProperties;


	public String getJdbcDriver() {
		return databaseProperties.getProperty(PropertyConstants.MYSQL_DRIVER_NAME);
	}

	public String getgenerateEntity() {
		return databaseProperties.getProperty(PropertyConstants.GENERATE_ENTITY_PATH);
	}

	public String getJdbcUser() {
		return environmentProperties.getBatchMySqlUsername();
	}
	
	public String getJdbcUrl() {
		String url = environmentProperties.getMysqlDataSourceUrl();
		url = url.replace(PropertyConstants.MAIN_APP_MYSQL_IP, environmentProperties.getBatchMySqlIp());
		url = url.replace(PropertyConstants.MAIN_APP_MYSQL_PORT, environmentProperties.getBatchMySqlPort());
		return url;
	}

	public String getJdbcPassword() {
		return environmentProperties.getBatchMySqlPassword();
	}

	public String getBatchDatabaseName() {
		return databaseProperties.getProperty(PropertyConstants.MYSQL_DATABASE_NAME);
	}

	public String getMysqlMinPoolSize() {
		return environmentProperties.getMysqlMinPoolSize();
	}

	public String getMysqlMaxPoolSize() {
		return environmentProperties.getMysqlMaxPoolSize();
	}

	public String getMysqlIdleConnectionTestPeriod() {
		return environmentProperties.getMysqlIdleConnectionTestPeriod();
	}

	public String getMysqlMaxIdleTimeOut() {
		return environmentProperties.getMysqlMaxIdleTimeout();
	}

	public String getMysqlMaxIdleTimeExcessConnections() {
		return environmentProperties.getMysqlMaxIdleTimeExcessConnections();
	}

	public String getMasterPreferredTestQuery() {
		return environmentProperties.getMasterPreferredTestQuery();
	}
	
}
