package com.lowcode.sqlconnection.property;

public class PropertyConstants {
	
	public static final String MAIN_APP_MYSQL_IP = "[MAIN_APP_MYSQL_IP]";
	public static final String MAIN_APP_MYSQL_PORT = "[MAIN_APP_MYSQL_PORT]";

	// MySql properties Name
	public static final String MYSQL_DATASOURCE_URL = "mainApp.mysql.DataSource.URL";
	public static final String MYSQL_DATABASE_NAME = "mainApp.mysql.databaseName";
	public static final String MYSQL_DRIVER_NAME = "mainApp.mysqlDriver";
	public static final String MYSQL_IP_ADDRESS = "mainApp.mysql.ip";
	public static final String MYSQL_PORT_NUMBER = "mainApp.mysql.port";
	public static final String MYSQL_USER_NAME = "mainApp.mysql.username";
	public static final String MYSQL_PASSWORD = "mainApp.mysql.password";
	public static final String MYSQL_PREFERRED_QUERY = "mainApp.mysql.preferredQuery";
	public static final String MYSQL_CONNECTION_CHECKOUT = "mainApp.mysql.connectionCheckout";

	public static final String MYSQL_MIN_POOL_SIZE = "mainApp.mysql.minPoolsize";
	public static final String MYSQL_MAX_POOL_SIZE = "mainApp.mysql.maxPoolSize";
	public static final String MYSQL_IDLE_CONNECTION_TEST_PERIOD = "mainApp.mysql.idleConnectionTestperiod";
	public static final String MYSQL_MAX_IDLE_TIMEOUT = "mainApp.mysql.maxIdleTimeout";
	public static final String MYSQL_MAX_IDLE_TIME_EXCESSCONNECTIONS = "mainApp.mysql.maxIdleExcessConnections";
	public static final String GENERATE_ENTITY_PATH = "dyapp.generateEntity";
}
