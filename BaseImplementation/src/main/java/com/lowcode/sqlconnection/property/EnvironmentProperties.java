package com.lowcode.sqlconnection.property;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;


@Configuration
@PropertySource("classpath:/environment.properties")
@PropertySource(value = "file:///${LOWCODE_ENVIRONMENT_PROPERTIES}/environment.properties", ignoreResourceNotFound = true)
public class EnvironmentProperties {

	@Autowired
	private Environment environment;

	public String getBatchMySqlIp() {
		return environment.getProperty(PropertyConstants.MYSQL_IP_ADDRESS);
	}

	public String getBatchMySqlPort() {
		return environment.getProperty(PropertyConstants.MYSQL_PORT_NUMBER);
	}

	public String getBatchMySqlUsername() {
		return environment.getProperty(PropertyConstants.MYSQL_USER_NAME);
	}

	public String getBatchMySqlPassword() {
		return environment.getProperty(PropertyConstants.MYSQL_PASSWORD);
	}

	public String getMasterPreferredTestQuery() {
		return environment.getProperty(PropertyConstants.MYSQL_PREFERRED_QUERY);
	}

	public String getMysqlMinPoolSize() {
		return environment.getProperty(PropertyConstants.MYSQL_MIN_POOL_SIZE);
	}

	public String getMysqlMaxPoolSize() {
		return environment.getProperty(PropertyConstants.MYSQL_MAX_POOL_SIZE);
	}

	public String getMysqlIdleConnectionTestPeriod() {
		return environment.getProperty(PropertyConstants.MYSQL_IDLE_CONNECTION_TEST_PERIOD);
	}

	public String getMysqlMaxIdleTimeout() {
		return environment.getProperty(PropertyConstants.MYSQL_MAX_IDLE_TIMEOUT);
	}

	public String getMysqlMaxIdleTimeExcessConnections() {
		return environment.getProperty(PropertyConstants.MYSQL_MAX_IDLE_TIME_EXCESSCONNECTIONS);
	}

	public String getMysqlDataSourceUrl() {
		return environment.getProperty(PropertyConstants.MYSQL_DATASOURCE_URL);
	}
	
	public String getgeneratEntity() {
		return environment.getProperty(PropertyConstants.GENERATE_ENTITY_PATH);
	}
}
