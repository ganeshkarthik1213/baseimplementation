package com.lowcode.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class LowCodeExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(LowCodeExceptionHandler.class);

	@ExceptionHandler(LowCodeException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody LowCodeException handleLowCodeException(LowCodeException ex) {
		logger.error("EazycomException ========> " + ex);
		return ex;
	}

}
