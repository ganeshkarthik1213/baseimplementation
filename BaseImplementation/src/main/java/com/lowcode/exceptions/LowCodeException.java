package com.lowcode.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lowcode.common.error.ErrorId;
import com.lowcode.common.error.ErrorIdList;


@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class LowCodeException extends Exception {
	private static final long serialVersionUID = 676852299677539062L;

	public LowCodeException() {
		super();
	}

	@JsonIgnore
	@Override
	public StackTraceElement[] getStackTrace() {
		return super.getStackTrace();
	}

	public LowCodeException(String message) {
		super(message);
	}

	public LowCodeException(ErrorIdList errorIdList) {
		super(errorIdList.convertToJsonString());
	}

	public LowCodeException(ErrorId errorId) {
		super(new ErrorIdList(errorId).convertToJsonString());
	}

	public static boolean isBlank(String input) {
		return (input == null || (input != null && input.trim().length() == 0)) ? true : false ;

	}
}
