package com.lowcode.exceptions;

import org.springframework.http.HttpStatus;

import com.lowcode.common.error.ErrorId;
import com.lowcode.common.error.ErrorIdList;



public class LowCodeUnauthorizedException extends LowCodeException {

	private static final long serialVersionUID = 674090085986146600L;

	private HttpStatus httpStatus;

	public LowCodeUnauthorizedException() {
		super();
	}

	public LowCodeUnauthorizedException(ErrorId errorId, HttpStatus httpStatus) {
		super(new ErrorIdList(errorId).convertToJsonString());
		this.httpStatus = httpStatus;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
}
