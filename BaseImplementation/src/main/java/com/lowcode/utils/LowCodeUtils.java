package com.lowcode.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.springframework.stereotype.Component;

@Component
public class LowCodeUtils {



	public static boolean isBlank(String input) {
		if (input == null || (input != null && input.trim().length() == 0)) {
			return true;
		}
		return false;
	}

	public static boolean isNotBlank(String input) {
		return !isBlank(input);
	}
//
//	public static String BCryptPassword(String originalPassword) throws LowCodeException {
//		if (originalPassword == null || originalPassword.isEmpty()) {
//			throw new LowCodeException(new ErrorId(ErrorCodes.INVALID_PASSWORD.getErrorCode(),
//					ErrorCodes.INVALID_PASSWORD.getErrorMessage()));
//		}
//		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//		return encoder.encode(originalPassword);
//	}

	public static boolean isValidList(List<?> input) {
		if (input != null && input.size() > 0) {
			return true;
		}
		return false;
	}
	
	public static boolean isValidSet(Set<?> input) {
		if (input != null && input.size() > 0) {
			return true;
		}
		return false;
	}
	
	public static String getUniqueDtringId() {
		int leftLimit = 97; // letter 'a'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = 10;
	    Random random = new Random();

	    String uniqueIdentity = random.ints(leftLimit, rightLimit + 1)
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();
		
		return uniqueIdentity;
	}
	
	public static boolean isValidIdentity(String identity) {
		if(identity != null && identity.trim().length()>0) {
			return true;
		}
		return false;
	}
	
	public static boolean isVaildIdentityCheck(String identity) {
		if(identity==null||identity=="") {
			return true;
		}
		return false;
	}
	
	public static String convertListToString(List<String> values) {
		if(!isValidList(values)) {
			return null;
		}
		StringBuilder str = new StringBuilder();
		values.forEach(a->str.append(a+","));
		return str.toString();
	}
	
	public static List<String> convertStringToList(String value) {
		if(isBlank(value)) {
			return null;
		}
		List<String> values = new ArrayList<>();
		String[] splittedStringArray = value.split(",");
		for(String str:splittedStringArray) {
			if(isNotBlank(str))
				values.add(str);
		}
		return values;
	}
	
//	public static String buildOptionsString(List<OptionValue> options) {
//		if(!isValidList(options)) {
//			return null;
//		}
//		StringBuilder str = new StringBuilder();
//		options.forEach(a->str.append(a.getValue()+","));
//		return str.toString();
//	}
//	
//	public static List<OptionValue> buildStringToOptions(String value) {
//		if(isBlank(value)) {
//			return null;
//		}
//		List<OptionValue> values = new ArrayList<>();
//		String[] splittedStringArray = value.split(",");
//		for(String str:splittedStringArray) {
//			if(isNotBlank(str)) {
//				OptionValue val = new OptionValue();
//				val.setValue(str);
//				values.add(val);
//			}
//				
//		}
//		return values;
//	}
//
//	public static List<OptionValue> buildStringListToOptionList(List<String> strList) {
//		if(!isValidList(strList)) {
//			return null;
//		}
//		List<OptionValue> values = new ArrayList<>();
//		for(String str : strList) {
//			if(isNotBlank(str)) {
//				OptionValue val = new OptionValue();
//				val.setValue(str);
//				val.setIsDefault(false);
//				val.setIdentity(str);
//				values.add(val);
//			}
//				
//		}
//		return values;
//	}

	
	public static Date stringToDateconvert(String value) {
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		try {
			return formatter.parse(value);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}  
		return null;
	}
	
	public static Date stringToDateconvertForGridFilter(String value) {
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd");
		try {
			return formatter.parse(value);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}  
		return null;
	}


	public static Date stringToDateWithFormat(String value, String format) {
		SimpleDateFormat formatter=new SimpleDateFormat(format);
		try {
			return formatter.parse(value);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}  
		return null;
	}

		
	public static Date stringToDateTimeconvert(String value) {
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm");

		try {
			return formatter.parse(value);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}  
		return null;
	}
	public static Date stringToDateTimeconvert_1(String value) {
		SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

		try {
			return formatter.parse(value);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}  
		return null;
	}

	public static String getCoventionalTimeZone(String timeZoneStr) {
		return timeZone.get(timeZoneStr) == null ? timeZoneStr : timeZone.get(timeZoneStr);
	}

	private static Map<String, String> timeZone = new HashMap<>();
	static {
		timeZone.put("IST", "Asia/Calcutta");
		timeZone.put("UTC", "UTC");
		timeZone.put("EST", "America/New_York");
		timeZone.put("CST", "America/Havana");
		// Europe/Copenhagen

	}
	
	public static Date stringToDateFieldconvert(String value) {
		SimpleDateFormat formatter=new SimpleDateFormat("M/d/yyyy");
		try {
			return formatter.parse(value);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}  
		return null;
	}


//	public static boolean isNotValidObject(Object obj) {
//
//		if (obj != null && obj != "") {
//			if (obj instanceof String && isBlank(obj.toString())) {
//				return true;
//
//			} else if (obj instanceof JSONArray) {
//				JSONArray arr = (JSONArray) obj;
//
//				if (arr.isEmpty()) {
//					return true;
//				}
//
//			} else if (obj instanceof ArrayList<?>) {
//
//				List<?> list = new ArrayList<>();
//				list = (List<?>) obj;
//
//				if (!isValidList(list)) {
//					return true;
//				}
//
//			}
//		} else {
//			return true;
//		}
//
//		return false;
//	}
//
//
//	public static List<HashMap<String, Object>> convertDocumentListToHashMapList(List<Document> mappedResults) {
//
//		List<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
//
//		if (LowCodeUtils.isValidList(mappedResults)) {
//			for (Document doc : mappedResults) {
//				HashMap<String, Object> object = new HashMap<String, Object>();
//				for (String key : doc.keySet()) {
//					object.put(key, doc.get(key));
//				}
//				result.add(object);
//			}
//		}
//		return result;
//	}
//
//	public static HashMap<String, Object> convertDocumentToHashMap(Document doc) {
//
//		HashMap<String, Object> result = new HashMap<String, Object>();
//
//		if (doc != null) {
//			for (String key : doc.keySet()) {
//				result.put(key, doc.get(key));
//			}
//		}
//
//		return result;
//	}

	public static String getCurrentWeekFirstDate(String format) {
		Calendar c = Calendar.getInstance();

		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

		DateFormat df = new SimpleDateFormat(format);
		return df.format(c.getTime());
	}

	public static String getCurrentWeekLastDate(String format) {
		Calendar c = Calendar.getInstance();

		c.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);

		DateFormat df = new SimpleDateFormat(format);
		return df.format(c.getTime());
	}

	public static String getLastWeekFirstDate(String format) {
		Calendar c = Calendar.getInstance();

		c.add(Calendar.WEEK_OF_YEAR, -1);
        c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

		DateFormat df = new SimpleDateFormat(format);
		return df.format(c.getTime());
	}

	public static String getLastWeekLastDate(String format) {
		Calendar c = Calendar.getInstance();

		c.add(Calendar.WEEK_OF_YEAR, -1);
		c.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);

		DateFormat df = new SimpleDateFormat(format);
		return df.format(c.getTime());
	}

	public static String getNextWeekFirstDate(String format) {
		Calendar c = Calendar.getInstance();

		c.add(Calendar.WEEK_OF_YEAR, 1);
        c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

		DateFormat df = new SimpleDateFormat(format);
		return df.format(c.getTime());
	}

	public static String getNextWeekLastDate(String format) {
		Calendar c = Calendar.getInstance();

		c.add(Calendar.WEEK_OF_YEAR, 1);
		c.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);

		DateFormat df = new SimpleDateFormat(format);
		return df.format(c.getTime());
	}

	public static String convertTimeStampToDate(String format, Object timestamp) {

		Date date=new Date((long) timestamp);

		DateFormat df = new SimpleDateFormat(format);
		return df.format(date);
	}


}
