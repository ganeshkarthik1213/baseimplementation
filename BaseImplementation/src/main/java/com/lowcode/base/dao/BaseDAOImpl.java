package com.lowcode.base.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;

import com.lowcode.common.filter.DataFilterFactory;
import com.lowcode.common.filter.DataFilterRegistry;
import com.lowcode.common.filter.FilterNameEnum;
import com.lowcode.exceptions.LowCodeException;
import com.lowcode.infrastructure.support.HibernateDAOSupport;


public abstract class BaseDAOImpl extends HibernateDAOSupport {

	@Autowired
	private DataFilterRegistry dataFilterRegistry;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private DataFilterFactory filterFactory;

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private HibernateTemplate hibernateTemplate;

	private static final String IDENTITY_SET_METHOD = "setIdentity";

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	protected void executeBulkUpdate(TypedQuery<?> typedQuery) {
		typedQuery.executeUpdate();
	}

	public void commitTransaction() {
		sessionFactory.getCurrentSession().getTransaction().commit();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	/**
	 * 
	 * @param typedQuery query for your entity
	 * @return returns the object for its executes correctly else its throws no
	 *         result exception returns null
	 */
	protected Object getSingleResult(TypedQuery<?> typedQuery) {
		Object object = null;

		try {
			object = typedQuery.getSingleResult();
		} catch (NoResultException e) {
			object = null;
		} /*
			
			 */
		return object;
	}

	/**
	 * 
	 * @param typedQuery query for your entity
	 * @return returns the List<object> for its executes correctly else its throws
	 *         no result exception returns new ArrayList<>()
	 */
	protected List<?> getResultList(Query typedQuery) {
		List<?> list = null;
		
		try {
			list = typedQuery.getResultList();
		} catch (NoResultException e) {
			list = new ArrayList<>();
		} /*

			 */
		return list;
	}

	protected NativeQuery<?> getQuery(String queryString) {
		return getSession().createNativeQuery(queryString);
	}


	protected Integer save(Object entity, String tableName) throws LowCodeException {

	setIdentityValue(entity, tableName);
	return (Integer) hibernateTemplate.save(entity);

	}
	private void setIdentityValue(Object entity, String tableName) throws LowCodeException {
		try {
			Method name = entity.getClass().getMethod(IDENTITY_SET_METHOD, String.class);
			name.invoke(entity, getNextSequence(tableName));
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			methodInvokeExhandler(e);
	}

	}

	protected void methodInvokeExhandler(Exception e) throws LowCodeException {

	}

	public String getNextSequence(String tableName) {

		/*
		 * StoredProcedureQuery sq =
		 * getSession().createStoredProcedureQuery(TmsConstants.
		 * NEXT_SEQUENCE_STORE_PROCEDURE)
		 * .registerStoredProcedureParameter(TmsConstants.NEXT_SEQUENCE_INPUT_PARAMETER,
		 * String.class, ParameterMode.IN)
		 * .registerStoredProcedureParameter(TmsConstants.
		 * NEXT_SEQUENCE_OUTPUT_PARAMETER, Integer.class, ParameterMode.OUT)
		 */
		/*
		 * Integer sequenceNumber = (Integer)
		 
		
		 */
		UUID uuid = UUID.randomUUID();
		return uuid.toString().replace("-", "");
		
	}

	protected CriteriaBuilder getCriteriaBuilder() {

		 return entityManager.getCriteriaBuilder();
	}

	protected TypedQuery<?> createQuery(CriteriaBuilder builder, CriteriaQuery<?> criteria, Root<?> root,
			List<Predicate> predicates) {

		if (dataFilterRegistry.getDataFilters(this) != null) {

			for (FilterNameEnum filter : dataFilterRegistry.getDataFilters(this).keySet()) {
				Predicate predicate = filterFactory.getFilterByName(filter).getFilter(root, this);
				if (predicate != null) {
					predicates.add(predicate);
				}
			}
		}
		if (predicates.size() > 0)
			criteria.where(builder.and(predicates.toArray(new Predicate[predicates.size()])));
		return entityManager.createQuery(criteria);

	}

	protected TypedQuery<?> createQuery(CriteriaBuilder builder,CriteriaUpdate<?> update,List<Predicate> predicates){
		update.where(builder.and(predicates.toArray(new Predicate[predicates.size()])));
		return getSession().createQuery(update);
	}
	
	

	

	
}