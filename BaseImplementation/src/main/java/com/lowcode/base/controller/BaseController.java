package com.lowcode.base.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.lowcode.model.LowCodeResponse;
import com.lowcode.exceptions.LowCodeUnauthorizedException;
import com.lowcode.model.User;
import com.lowcode.userManager.IuserManagerservice;
import com.lowcode.utils.LowCodeUtils;

@Component
public abstract class BaseController {

	@Autowired
	private IuserManagerservice userManager;

	protected Logger logger = LoggerFactory.getLogger(getClassName());

	private Map<String, String> jsonObject = new HashMap<String, String>();

	private final String SUCCESS = "Success!";

	protected abstract Class<?> getClassName();

	public User getLoggedInUser() throws LowCodeUnauthorizedException {
		return userManager.getLoggedInUser();
		
	}

	public ResponseEntity<LowCodeResponse> getDefaultResponseEntity() {
		return getResponseEntity(jsonObject, SUCCESS, HttpStatus.OK);
	}

	public ResponseEntity<LowCodeResponse> getResponseEntity(String status) {
		return getResponseEntity(jsonObject, status, HttpStatus.OK);
	}

	public ResponseEntity<LowCodeResponse> getResponseEntity(Object content) {
		return getResponseEntity(content, SUCCESS, HttpStatus.OK);
	}

	public ResponseEntity<LowCodeResponse> getResponseEntity(String status, HttpStatus httpStatus) {
		return getResponseEntity(jsonObject, status, httpStatus);
	}

	public ResponseEntity<LowCodeResponse> getResponseEntity(Object content, String status) {
		return getResponseEntity(content, status, HttpStatus.OK);
	}

	public ResponseEntity<LowCodeResponse> getResponseEntity(Object content, HttpStatus httpStatus) {
		return getResponseEntity(content, SUCCESS, httpStatus);
	}

	public ResponseEntity<LowCodeResponse> getResponseEntity(Object content, String status, HttpStatus httpStatus) {
		LowCodeResponse LowCodeResponse = new LowCodeResponse();
		LowCodeResponse.setContent(content);
		if (LowCodeUtils.isBlank(status)) {
			LowCodeResponse.setStatus(status);
		}
		return new ResponseEntity<LowCodeResponse>(LowCodeResponse, httpStatus);
	}

	public LowCodeResponse getLowCodeResponse(Object content) {
		LowCodeResponse LowCodeResponse = new LowCodeResponse();
		if (content != null) {
			LowCodeResponse.setContent(content);
		}
		return LowCodeResponse;
	}

	public LowCodeResponse getLowCodeResponse(Object content, HttpStatus httpStatus) {
		LowCodeResponse LowCodeResponse = new LowCodeResponse();
		if (content != null) {
			LowCodeResponse.setContent(content);
			LowCodeResponse.setStatus(HttpStatus.OK);
		}
		return LowCodeResponse;
	}

	public LowCodeResponse getLowCodeResponse(HttpStatus httpStatus) {
		LowCodeResponse LowCodeResponse = new LowCodeResponse();
		LowCodeResponse.setStatus(httpStatus);
		return LowCodeResponse;
	}

	public LowCodeResponse getDefaultLowCodeResponse() {
		LowCodeResponse LowCodeResponse = new LowCodeResponse();
		LowCodeResponse.setStatus(HttpStatus.OK);
		LowCodeResponse.setContent("success");
		return LowCodeResponse;
	}

	public IuserManagerservice getUserManager() {
		return userManager;
	}

	public void setUserManager(IuserManagerservice userManager) {
		this.userManager = userManager;
	}
	
	
}
