package com.lowcode.userManager;

import java.util.List;

import com.lowcode.exceptions.LowCodeException;
import com.lowcode.exceptions.LowCodeUnauthorizedException;
import com.lowcode.model.User;
import com.lowcode.model.UserGroup;

public interface IUserDao {
	
	String getLoggedInUserName() throws LowCodeUnauthorizedException;

	User getUser(String userName);

	User saveUser(User user) throws LowCodeException;

	List<User> getuserbysearch(String searchtext);

	List<UserGroup> getuserGroupbysearch(String searchtext);

	List<User> getAllUser();

	List<User> getUsersByUserIdenties(String[] userIds);

	List<User> getUnMatchedUsersByIds(List<Integer> ids);

	User getUserById(Integer id);

	List<User> getUserByUserName(String userName);
}
