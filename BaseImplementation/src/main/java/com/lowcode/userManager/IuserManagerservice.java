package com.lowcode.userManager;


import com.lowcode.exceptions.LowCodeException;
import com.lowcode.exceptions.LowCodeUnauthorizedException;
import com.lowcode.model.User;

public interface IuserManagerservice {
	User getLoggedInUser() throws LowCodeUnauthorizedException;

	void testHibernate() throws LowCodeException;

	void setUserDao(IUserDao iUserDao);
}