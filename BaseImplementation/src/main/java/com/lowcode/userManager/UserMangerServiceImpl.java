package com.lowcode.userManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.lowcode.common.error.ErrorCodes;
import com.lowcode.common.error.ErrorId;
import com.lowcode.common.error.ErrorId.Severity;
import com.lowcode.exceptions.LowCodeException;
import com.lowcode.exceptions.LowCodeUnauthorizedException;
import com.lowcode.model.User;

@Service
@Transactional(value = "transactionManager", readOnly = false)
public class UserMangerServiceImpl implements IuserManagerservice {
	private static final Logger logger = LoggerFactory.getLogger(UserMangerServiceImpl.class);

	@Autowired
	private IUserDao iUserDao;

	@Override
	public User getLoggedInUser() throws LowCodeUnauthorizedException {
		String userName = iUserDao.getLoggedInUserName();
		User appUser = getUser(userName);
		if (appUser == null) {
			logger.error("App user not found for given user name. Given user name is :: " + userName);
			throw new LowCodeUnauthorizedException(new ErrorId(ErrorCodes.INVALID_USER.getErrorCode(),
					ErrorCodes.INTERNAL_ERROR.getErrorMessage(), Severity.FATAL), HttpStatus.UNAUTHORIZED);
		}
		return appUser;
	}

	public User getUser(String userName) {
		return iUserDao.getUser(userName);
		 
	}

	@Override

	public void testHibernate() throws LowCodeException {
		User user = new User();
		user.setPassword("asdasdasd");
		iUserDao.saveUser(user);
	}

	public void setUserDao(IUserDao iUserDaoMock) {
	this.iUserDao=iUserDaoMock;
		
	}

}
