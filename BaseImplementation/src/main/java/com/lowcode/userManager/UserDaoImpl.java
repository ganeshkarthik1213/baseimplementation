package com.lowcode.userManager;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.lowcode.base.dao.BaseDAOImpl;
import com.lowcode.common.constant.Constants;
import com.lowcode.common.filter.DataFilterRegistry;
import com.lowcode.common.filter.FilterNameEnum;
import com.lowcode.exceptions.LowCodeException;
import com.lowcode.exceptions.LowCodeUnauthorizedException;
import com.lowcode.model.User;
import com.lowcode.model.UserGroup;
import com.lowcode.utils.LowCodeUtils;

@Repository
public class UserDaoImpl extends BaseDAOImpl implements IUserDao {
	public static final String USER_NAME = "userName";
	
	private User user;

	@Autowired
	private DataFilterRegistry dataFilterRegistry;
	

	@PostConstruct
	public void registerDataFilters() {
		dataFilterRegistry.add(this, FilterNameEnum.BUISNESS_FILTER, "buisnessId.identity"); 
	}

	@Override
	public String getLoggedInUserName() throws LowCodeUnauthorizedException {
		return super.getLoggedInUserName();
	}

	@Override
	public User getUser(String userName) {
		return getByField(User.class, USER_NAME, userName);
	
	}

	@Override

	public User saveUser(User user) throws LowCodeException {
		save(user, "user");
		return user;
	}

	@Override
	public List<User> getUserByUserName(String userName) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<User> criteria = builder.createQuery(User.class);
		Root<User> root = criteria.from(User.class);
		criteria.select(root);
		
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(builder.and(builder.equal(root.get("isDeleted"), false)));
		predicates.add(builder.and(builder.equal(root.get("isImportedUser"), false)));
		predicates.add(builder.and(builder.equal(root.get("userName"), userName)));

		return (List<User>) getResultList(createQuery(builder, criteria, root, predicates));
	
	}

	@Override
	public List<User> getuserbysearch(String searchtext) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<User> criteria = builder.createQuery(User.class);
		Root<User> root = criteria.from(User.class);
		criteria.select(root);
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(builder.and(builder.equal(root.get("isDeleted"), false)));
		predicates.add(builder.and(builder.equal(root.get("isImportedUser"), false)));
		predicates.add(builder.and(builder.like(root.get("userName"), searchtext+ Constants.PERCENTAGE)));


		return (List<User>) getResultList(createQuery(builder, criteria, root, predicates));
	
	}

	@Override
	public List<User> getAllUser() {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<User> criteria = builder.createQuery(User.class);
		Root<User> root = criteria.from(User.class);
		criteria.select(root);
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(builder.and(builder.equal(root.get("isDeleted"), false)));
		predicates.add(builder.and(builder.equal(root.get("isImportedUser"), false)));
		return (List<User>) getResultList(createQuery(builder, criteria, root, predicates));
	
	}
	
	
	@Override
	public List<User> getUsersByUserIdenties(String[] userIds) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<User> criteria = builder.createQuery(User.class);
		Root<User> root = criteria.from(User.class);
		criteria.select(root);
		List<Predicate> predicates = new ArrayList<>();
		Expression<String> expression = root.get("identity");				
		predicates.add(builder.and(expression.in(userIds)));
		predicates.add(builder.and(builder.equal(root.get("isDeleted"), false)));
		predicates.add(builder.and(builder.equal(root.get("isImportedUser"), false)));
		return (List<User>) getResultList(createQuery(builder, criteria, root, predicates));
	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserGroup> getuserGroupbysearch(String searchtext) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<UserGroup> criteria = builder.createQuery(UserGroup.class);
		Root<UserGroup> root = criteria.from(UserGroup.class);
		criteria.select(root);
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(builder.and(builder.equal(root.get("isDeleted"), false)));
		predicates.add(builder.and(builder.equal(root.get("isImportedUser"), false)));
		predicates.add(builder.and(builder.like(root.get("groupName"), searchtext+ Constants.PERCENTAGE)));

		return (List<UserGroup>) getResultList(createQuery(builder, criteria, root, predicates));
	}
	
	@Override
	public List<User> getUnMatchedUsersByIds(List<Integer> ids) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<User> criteria = builder.createQuery(User.class);
		Root<User> root = criteria.from(User.class);
		criteria.select(root);
		
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(builder.and(builder.equal(root.get("isDeleted"), false)));
		predicates.add(builder.and(builder.equal(root.get("isImportedUser"), false)));
		if(LowCodeUtils.isValidList(ids)) {
			predicates.add(builder.not(root.get("id").in(ids)));			
		}

		return (List<User>) getResultList(createQuery(builder, criteria, root, predicates));
	
	}

	@Override
	public User getUserById(Integer id) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<User> criteria = builder.createQuery(User.class);
		Root<User> root = criteria.from(User.class);
		criteria.select(root);
		
		List<Predicate> predicates = new ArrayList<>();
		predicates.add(builder.and(builder.equal(root.get("isDeleted"), false)));
		predicates.add(builder.and(builder.equal(root.get("isImportedUser"), false)));
		predicates.add(builder.and(builder.equal(root.get("id"), id)));

		return (User) getSingleResult(createQuery(builder, criteria, root, predicates));
	
	}

}
