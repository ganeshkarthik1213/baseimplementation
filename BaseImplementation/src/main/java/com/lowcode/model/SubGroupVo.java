package com.lowcode.model;

import java.util.List;

public class SubGroupVo {
	private List<UserRoleVo> roles;
	private List<CustomGroupFilter> customGroup;
	private String subGroupCondition;
	private Integer subGroup;
	private String identity;
	private String subGroupRedIdentity;
	private String multiselectJson;
	/**
	 * @return the subGroupRedIdentity
	 */
	public String getSubGroupRedIdentity() {
		return subGroupRedIdentity;
	}
	/**
	 * @param subGroupRedIdentity the subGroupRedIdentity to set
	 */
	public void setSubGroupRedIdentity(String subGroupRedIdentity) {
		this.subGroupRedIdentity = subGroupRedIdentity;
	}
	/**
	 * @return the multiselectJson
	 */
	public String getMultiselectJson() {
		return multiselectJson;
	}
	/**
	 * @param multiselectJson the multiselectJson to set
	 */
	public void setMultiselectJson(String multiselectJson) {
		this.multiselectJson = multiselectJson;
	}
	/**
	 * @return the identity
	 */
	public String getIdentity() {
		return identity;
	}
	/**
	 * @param identity the identity to set
	 */
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	/**
	 * @return the roles
	 */
	public List<UserRoleVo> getRoles() {
		return roles;
	}
	/**
	 * @param roles the roles to set
	 */
	public void setRoles(List<UserRoleVo> roles) {
		this.roles = roles;
	}
	
	/**
	 * @return the customGroup
	 */
	public List<CustomGroupFilter> getCustomGroup() {
		return customGroup;
	}
	/**
	 * @param customGroup the customGroup to set
	 */
	public void setCustomGroup(List<CustomGroupFilter> customGroup) {
		this.customGroup = customGroup;
	}

	/**
	 * @return the subGroupCondition
	 */
	public String getSubGroupCondition() {
		return subGroupCondition;
	}
	/**
	 * @param subGroupCondition the subGroupCondition to set
	 */
	public void setSubGroupCondition(String subGroupCondition) {
		this.subGroupCondition = subGroupCondition;
	}
	/**
	 * @return the subGroup
	 */
	public Integer getSubGroup() {
		return subGroup;
	}
	/**
	 * @param subGroup the subGroup to set
	 */
	public void setSubGroup(Integer subGroup) {
		this.subGroup = subGroup;
	}
	
}
