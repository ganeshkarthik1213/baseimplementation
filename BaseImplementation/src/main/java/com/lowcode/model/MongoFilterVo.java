package com.lowcode.model;

import java.util.List;

import com.lowcode.model.LookUpFilterVo;

public class MongoFilterVo {

	private String collectionName;
	private List<String> columnNames;
	private List<MongoConditionVo> conditions;
	private String sortType;
	private String sortColumn;
	private Integer limit;
	private Integer skip;
	private Integer dataFilterId;
	private List<LookUpFilterVo> lookUp;
	private Integer collectionId;
	private boolean skipcheck;
	
	
	public void setDataFilterId(Integer dataFilterId) {
		this.dataFilterId = dataFilterId;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}

	public List<String> getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(List<String> columnNames) {
		this.columnNames = columnNames;
	}

	public List<MongoConditionVo> getConditions() {
		return conditions;
	}

	public void setConditions(List<MongoConditionVo> conditions) {
		this.conditions = conditions;
	}

	public String getSortType() {
		return sortType;
	}

	public void setSortType(String sortType) {
		this.sortType = sortType;
	}

	public String getSortColumn() {
		return sortColumn;
	}

	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getSkip() {
		return skip;
	}

	public void setSkip(Integer skip) {
		this.skip = skip;
	}
	

	/**
	 * @return the dataFilterId
	 */
	public Integer getDataFilterId() {
		return dataFilterId;
	}

	/**
	 * @return the lookUp
	 */
	public List<LookUpFilterVo> getLookUp() {
		return lookUp;
	}

	/**
	 * @param lookUp the lookUp to set
	 */
	public void setLookUp(List<LookUpFilterVo> lookUp) {
		this.lookUp = lookUp;
	}

	public Integer getCollectionId() {
		return collectionId;
	}

	public void setCollectionId(Integer collectionId) {
		this.collectionId = collectionId;
	}

	public boolean getSkipcheck() {
		return skipcheck;
	}

	public void setSkipcheck(boolean skipcheck) {
		this.skipcheck = skipcheck;
	}



	/**
	 * @param dataFilterId the dataFilterId to set
	 */

	
}
