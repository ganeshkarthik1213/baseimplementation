package com.lowcode.model;
// Generated 25 May, 2021 5:48:18 AM by Hibernate Tools 5.2.12.Final

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * UserGroup generated by hbm2java
 */
@Entity
@Table(name = "user_group")
public class UserGroup implements java.io.Serializable {

	private Integer id;
	private User userByModifiedBy;
	private User userByCreatedBy;
	private String groupName;
	private Date createdDate;
	private Date modifiedDate;
	private Boolean isDeleted;
	private String identity;
	private Set<UserGroupMapping> userGroupMappings = new HashSet<UserGroupMapping>(0);

	public UserGroup() {
	}

	public UserGroup(String identity) {
		this.identity = identity;
	}

	public UserGroup(User userByModifiedBy, User userByCreatedBy, String groupName, Date createdDate, Date modifiedDate,
			Boolean isDeleted, String identity, Set<UserGroupMapping> userGroupMappings) {
		this.userByModifiedBy = userByModifiedBy;
		this.userByCreatedBy = userByCreatedBy;
		this.groupName = groupName;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.isDeleted = isDeleted;
		this.identity = identity;
		this.userGroupMappings = userGroupMappings;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MODIFIED_BY")
	public User getUserByModifiedBy() {
		return this.userByModifiedBy;
	}

	public void setUserByModifiedBy(User userByModifiedBy) {
		this.userByModifiedBy = userByModifiedBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CREATED_BY")
	public User getUserByCreatedBy() {
		return this.userByCreatedBy;
	}

	public void setUserByCreatedBy(User userByCreatedBy) {
		this.userByCreatedBy = userByCreatedBy;
	}

	@Column(name = "GROUP_NAME", length = 45)
	public String getGroupName() {
		return this.groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE", length = 19)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_DATE", length = 19)
	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Column(name = "IS_DELETED")
	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Column(name = "IDENTITY", nullable = false)
	public String getIdentity() {
		return this.identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "userGroup")
	public Set<UserGroupMapping> getUserGroupMappings() {
		return this.userGroupMappings;
	}

	public void setUserGroupMappings(Set<UserGroupMapping> userGroupMappings) {
		this.userGroupMappings = userGroupMappings;
	}

}
