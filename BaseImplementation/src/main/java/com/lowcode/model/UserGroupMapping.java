package com.lowcode.model;
// Generated 25 May, 2021 5:48:18 AM by Hibernate Tools 5.2.12.Final

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * UserGroupMapping generated by hbm2java
 */
@Entity
@Table(name = "user_group_mapping")
public class UserGroupMapping implements java.io.Serializable {

	private Integer id;
	private User userByCreatedBy;
	private User userByUserId;
	private User userByModifiedBy;
	private UserGroup userGroup;
	private Date createdDate;
	private Date modifiedDate;
	private Boolean isDeleted;
	private String identity;

	public UserGroupMapping() {
	}

	public UserGroupMapping(Integer id, String identity) {
		this.id = id;
		this.identity = identity;
	}

	public UserGroupMapping(Integer id, User userByCreatedBy, User userByUserId, User userByModifiedBy, UserGroup userGroup,
			Date createdDate, Date modifiedDate, Boolean isDeleted, String identity) {
		this.id = id;
		this.userByCreatedBy = userByCreatedBy;
		this.userByUserId = userByUserId;
		this.userByModifiedBy = userByModifiedBy;
		this.userGroup = userGroup;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.isDeleted = isDeleted;
		this.identity = identity;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CREATED_BY")
	public User getUserByCreatedBy() {
		return this.userByCreatedBy;
	}

	public void setUserByCreatedBy(User userByCreatedBy) {
		this.userByCreatedBy = userByCreatedBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	public User getUserByUserId() {
		return this.userByUserId;
	}

	public void setUserByUserId(User userByUserId) {
		this.userByUserId = userByUserId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MODIFIED_BY")
	public User getUserByModifiedBy() {
		return this.userByModifiedBy;
	}

	public void setUserByModifiedBy(User userByModifiedBy) {
		this.userByModifiedBy = userByModifiedBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_GROUP_ID")
	public UserGroup getUserGroup() {
		return this.userGroup;
	}

	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_DATE", length = 19)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_DATE", length = 19)
	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Column(name = "IS_DELETED")
	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Column(name = "IDENTITY", nullable = false)
	public String getIdentity() {
		return this.identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

}
