package com.lowcode.model;

import java.util.List;

public class DataFilterVo {
	private Integer columnid;
	private String columnName;
	private String selectedOperator;
	private String fieldType;
	private String selectedOptionsType;
	private String useParams;
	private String useParamId;
	private Object value1;
	private String value2;
	private Integer subGroup;
	private Integer sequence;
	private String filterCondition;
	private String identity;
	private List<OptionValue> selectedOptionValues;
	private String alterFieldId;
	private Object columnId;
	private Boolean reference;
	private String referenceTableIdentity;
	private String referenceColumnIdentity;
	private String referenceColumnname; 
	private String referenceTableName;
	private String selectedOptionName;
	private Integer dynamicMonth;
	
	
	
	
	public Integer getDynamicMonth() {
		return dynamicMonth;
	}
	public void setDynamicMonth(Integer dynamicMonth) {
		this.dynamicMonth = dynamicMonth;
	}
	private String userRefColIdentity;
	public String getReferenceColumnIdentity() {
		return referenceColumnIdentity;
	}
	public void setReferenceColumnIdentity(String referenceColumnIdentity) {
		this.referenceColumnIdentity = referenceColumnIdentity;
	}
	public Boolean getReference() {
		return reference;
	}
	public void setReference(Boolean reference) {
		this.reference = reference;
	}
	public String getReferenceTableIdentity() {
		return referenceTableIdentity;
	}
	public void setReferenceTableIdentity(String referenceTableIdentity) {
		this.referenceTableIdentity = referenceTableIdentity;
	}
	/**
	 * @return the selectedOptionValues
	 */
	public List<OptionValue> getSelectedOptionValues() {
		return selectedOptionValues;
	}
	/**
	 * @param selectedOptionValues the selectedOptionValues to set
	 */
	public void setSelectedOptionValues(List<OptionValue> selectedOptionValues) {
		this.selectedOptionValues = selectedOptionValues;
	}
	/**
	 * @return the identity
	 */
	public String getIdentity() {
		return identity;
	}
	/**
	 * @param identity the identity to set
	 */
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	
	public Integer getColumnid() {
		return columnid;
	}
	public void setColumnid(Integer columnid) {
		this.columnid = columnid;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	/**
	 * @return the fieldId
	 */
	/**
	 * @return the selectedOperator
	 */
	public String getSelectedOperator() {
		return selectedOperator;
	}
	/**
	 * @param selectedOperator the selectedOperator to set
	 */
	public void setSelectedOperator(String selectedOperator) {
		this.selectedOperator = selectedOperator;
	}
	/**
	 * @return the fieldType
	 */
	public String getFieldType() {
		return fieldType;
	}
	/**
	 * @param fieldType the fieldType to set
	 */
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	/**
	 * @return the selectedOptionsType
	 */
	public String getSelectedOptionsType() {
		return selectedOptionsType;
	}
	/**
	 * @param selectedOptionsType the selectedOptionsType to set
	 */
	public void setSelectedOptionsType(String selectedOptionsType) {
		this.selectedOptionsType = selectedOptionsType;
	}
	/**
	 * @return the useParams
	 */
	public String getUseParams() {
		return useParams;
	}
	/**
	 * @param useParams the useParams to set
	 */
	public void setUseParams(String useParams) {
		this.useParams = useParams;
	}
	/**
	 * @return the useParamId
	 */
	public String getUseParamId() {
		return useParamId;
	}
	/**
	 * @param useParamId the useParamId to set
	 */
	public void setUseParamId(String useParamId) {
		this.useParamId = useParamId;
	}
	/**
	 * @return the value1
	 */
	public Object getValue1() {
		return value1;
	}
	
	/**
	 * @param value1 the value1 to set
	 */
	public void setValue1(Object value1) {
		this.value1 = value1;
	}
	/**
	 * @return the value2
	 */
	public String getValue2() {
		return value2;
	}
	/**
	 * @param value2 the value2 to set
	 */
	public void setValue2(String value2) {
		this.value2 = value2;
	}
	/**
	 * @return the subGroup
	 */
	public Integer getSubGroup() {
		return subGroup;
	}
	/**
	 * @param subGroup the subGroup to set
	 */
	public void setSubGroup(Integer subGroup) {
		this.subGroup = subGroup;
	}
	/**
	 * @return the sequence
	 */
	public Integer getSequence() {
		return sequence;
	}
	/**
	 * @param sequence the sequence to set
	 */
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	/**
	 * @return the filterCondition
	 */
	public String getFilterCondition() {
		return filterCondition;
	}
	/**
	 * @param filterCondition the filterCondition to set
	 */
	public void setFilterCondition(String filterCondition) {
		this.filterCondition = filterCondition;
	}
	public String getAlterFieldId() {
		return alterFieldId;
	}
	public void setAlterFieldId(String alterFieldId) {
		this.alterFieldId = alterFieldId;
	}
	public Object getColumnId() {
		return columnId;
	}
	public void setColumnId(Object columnId) {
		this.columnId = columnId;
	}
	public String getReferenceColumnname() {
		return referenceColumnname;
	}
	public void setReferenceColumnname(String referenceColumnname) {
		this.referenceColumnname = referenceColumnname;
	}
	public String getReferenceTableName() {
		return referenceTableName;
	}
	public void setReferenceTableName(String referenceTableName) {
		this.referenceTableName = referenceTableName;
	}
	public String getSelectedOptionName() {
		return selectedOptionName;
	}
	public void setSelectedOptionName(String selectedOptionName) {
		this.selectedOptionName = selectedOptionName;
	}
	
	/**
	 * @return the userRefColIdentity
	 */
	public String getUserRefColIdentity() {
		return userRefColIdentity;
	}
	/**
	 * @param userRefColIdentity the userRefColIdentity to set
	 */
	public void setUserRefColIdentity(String userRefColIdentity) {
		this.userRefColIdentity = userRefColIdentity;
	}	
	
	
	
}