package com.lowcode.model;

import java.util.Map;

public class OptionValue {
    private String value;
    private Object identity;
    private Integer columnDetailId;
    private Map<String, Object> otherValues;
    /**
	 * @return the identity
	 */
	public Object getIdentity() {
		return identity;
	}

	/**
	 * @param identity the identity to set
	 */
	public void setIdentity(Object identity) {
		this.identity = identity;
	}

	private Boolean isDefault;
    private Integer OptionType;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}




	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	/**
	 * @return the optionType
	 */
	public Integer getOptionType() {
		return OptionType;
	}

	/**
	 * @param optionType the optionType to set
	 */
	public void setOptionType(Integer optionType) {
		OptionType = optionType;
	}


	public Integer getColumnDetailId() {
		return columnDetailId;
	}

	public void setColumnDetailId(Integer columnDetailId) {
		this.columnDetailId = columnDetailId;
	}

	/**
	 * @return the otherValues
	 */
	public Map<String, Object> getOtherValues() {
		return otherValues;
	}

	/**
	 * @param otherValues the otherValues to set
	 */
	public void setOtherValues(Map<String, Object> otherValues) {
		this.otherValues = otherValues;
	}

	
    
}
