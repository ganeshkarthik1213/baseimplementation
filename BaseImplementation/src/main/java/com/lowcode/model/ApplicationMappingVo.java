package com.lowcode.model;

import java.util.List;

public class ApplicationMappingVo {
    
	private String identity;			// ApplicationMapping table identity 
	private String name;				// Application name
	private int referenceId; 			// UserId or user role id . Based on reference for
	private int referenceFor; 		// User or UserRole . Refer ReferenceFor.enum
	private String applicationId; 	// Id of the application
	private boolean isDeleted;
	private List<FunctionalityPagesMappingVo> mappedPageObj;
	private String landingPageId;
	private String landingMenuId;
	
	public String getLandingMenuId() {
		return landingMenuId;
	}
	public void setLandingMenuId(String landingMenuId) {
		this.landingMenuId = landingMenuId;
	}
	public String getIdentity() {
		return identity;
	}
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(int referenceId) {
		this.referenceId = referenceId;
	}
	public int getReferenceFor() {
		return referenceFor;
	}
	public void setReferenceFor(int referenceFor) {
		this.referenceFor = referenceFor;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public List<FunctionalityPagesMappingVo> getMappedPageObj() {
		return mappedPageObj;
	}
	public void setMappedPageObj(List<FunctionalityPagesMappingVo> mappedPageObj) {
		this.mappedPageObj = mappedPageObj;
	}
	public String getLandingPageId() {
		return landingPageId;
	}
	public void setLandingPageId(String landingPageId) {
		this.landingPageId = landingPageId;
	}
	
	
	
	
}
