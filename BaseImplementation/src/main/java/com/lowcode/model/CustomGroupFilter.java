package com.lowcode.model;

import java.util.List;

public class CustomGroupFilter {
	private List<DataFilterVo> filters;
	private String customGroupCondition;
	private Integer customGroupId;
	/**
	 * @return the filters
	 */
	public List<DataFilterVo> getFilters() {
		return filters;
	}
	/**
	 * @param filters the filters to set
	 */
	public void setFilters(List<DataFilterVo> filters) {
		this.filters = filters;
	}
	/**
	 * @return the customGroupcondition
	 */
	public String getCustomGroupCondition() {
		return customGroupCondition;
	}
	/**
	 * @param customGroupcondition the customGroupcondition to set
	 */
	public void setCustomGroupCondition(String customGroupcondition) {
		this.customGroupCondition = customGroupcondition;
	}
	/**
	 * @return the customGroupId
	 */
	public Integer getCustomGroupId() {
		return customGroupId;
	}
	/**
	 * @param customGroupId the customGroupId to set
	 */
	public void setCustomGroupId(Integer customGroupId) {
		this.customGroupId = customGroupId;
	}
}