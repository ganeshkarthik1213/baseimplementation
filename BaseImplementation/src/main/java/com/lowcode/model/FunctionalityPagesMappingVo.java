package com.lowcode.model;

import java.util.List;

public class FunctionalityPagesMappingVo {
	
	private String mappingIdentity;
	private Integer referenceFor;
	private Integer referenceId;
	private boolean isMapped;
	
	private String applicationIdentity ;
	private String pageName;
	private String pageIdentity;
	private boolean rolePageMapped;
	private boolean defaultLandingPage;
	private boolean isSubPage;
	private boolean pageDeleted;
	private Integer menuId;
	private boolean isMenuPage;
	private String firstLogin;
	private String secondLogin;
	private boolean tabview = false;
	public boolean isTabview() {
		return tabview;
	}
	public void setTabview(boolean tabview) {
		this.tabview = tabview;
	}
	public String getFirstLogin() {
		return firstLogin;
	}
	public void setFirstLogin(String firstLogin) {
		this.firstLogin = firstLogin;
	}
	public String getSecondLogin() {
		return secondLogin;
	}
	public void setSecondLogin(String secondLogin) {
		this.secondLogin = secondLogin;
	}
	public boolean isMenuPage() {
		return isMenuPage;
	}
	public void setMenuPage(boolean isMenuPage) {
		this.isMenuPage = isMenuPage;
	}
	public Integer getMenuId() {
		return menuId;
	}
	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}
	private List<FunctionalityPagesMappingVo> children;
	
	public List<FunctionalityPagesMappingVo> getChildren() {
		return children;
	}
	public void setChildren(List<FunctionalityPagesMappingVo> children) {
		this.children = children;
	}
	public String getMappingIdentity() {
		return mappingIdentity;
	}
	public void setMappingIdentity(String mappingIdentity) {
		this.mappingIdentity = mappingIdentity;
	}
	public Integer getReferenceFor() {
		return referenceFor;
	}
	public void setReferenceFor(Integer referenceFor) {
		this.referenceFor = referenceFor;
	}
	public Integer getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(Integer referenceId) {
		this.referenceId = referenceId;
	}
	public boolean isMapped() {
		return isMapped;
	}
	public void setMapped(boolean isMapped) {
		this.isMapped = isMapped;
	}
	
	public String getApplicationIdentity() {
		return applicationIdentity;
	}
	public void setApplicationIdentity(String applicationIdentity) {
		this.applicationIdentity = applicationIdentity;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getPageIdentity() {
		return pageIdentity;
	}
	public void setPageIdentity(String pageIdentity) {
		this.pageIdentity = pageIdentity;
	}
	public boolean isRolePageMapped() {
		return rolePageMapped;
	}
	public void setRolePageMapped(boolean rolePageMapped) {
		this.rolePageMapped = rolePageMapped;
	}
	public boolean isDefaultLandingPage() {
		return defaultLandingPage;
	}
	public void setDefaultLandingPage(boolean defaultLandingPage) {
		this.defaultLandingPage = defaultLandingPage;
	}
	public boolean getIsSubPage() {
		return isSubPage;
	}
	public void setIsSubPage(boolean isSubPage) {
		this.isSubPage = isSubPage;
	}
	public boolean getPageDeleted() {
		return pageDeleted;
	}
	public void setPageDeleted(boolean pageDeleted) {
		this.pageDeleted = pageDeleted;
	}
	
	

}
