package com.lowcode.model;

public class LookUpFilterVo {

	private String fromCollection;
	private String fromColumnId;
	private String localColumnId;
	private String reference;
	/**
	 * @return the fromCollection
	 */
	public String getFromCollection() {
		return fromCollection;
	}
	/**
	 * @param fromCollection the fromCollection to set
	 */
	public void setFromCollection(String fromCollection) {
		this.fromCollection = fromCollection;
	}
	
	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}
	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}
	/**
	 * @return the fromColumnId
	 */
	public String getFromColumnId() {
		return fromColumnId;
	}
	/**
	 * @param fromColumnId the fromColumnId to set
	 */
	public void setFromColumnId(String fromColumnId) {
		this.fromColumnId = fromColumnId;
	}
	/**
	 * @return the localColumnId
	 */
	public String getLocalColumnId() {
		return localColumnId;
	}
	/**
	 * @param localColumnId the localColumnId to set
	 */
	public void setLocalColumnId(String localColumnId) {
		this.localColumnId = localColumnId;
	}
	
	
}
