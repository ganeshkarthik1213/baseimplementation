package com.lowcode.model;

import java.util.List;

public class MongoConditionVo {
	private String actualName;
	private String columnName;
	private String filterType;
	private Object value1;
	private Object value2;
	private List<?> values;
	private String referenceCollectionIdentity;
	private Integer referenceColumnId;
	private String columnType;

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getFilterType() {
		return filterType;
	}

	public void setFilterType(String filterType) {
		this.filterType = filterType;
	}

	public Object getValue1() {
		return value1;
	}

	public void setValue1(Object value1) {
		this.value1 = value1;
	}

	public Object getValue2() {
		return value2;
	}

	public void setValue2(Object value2) {
		this.value2 = value2;
	}

	public List<?> getValues() {
		return values;
	}

	public void setValues(List<?> values) {
		this.values = values;
	}

	/**
	 * @return the referenceCollectionIdentity
	 */
	public String getReferenceCollectionIdentity() {
		return referenceCollectionIdentity;
	}

	/**
	 * @return the referenceColumnId
	 */
	public Integer getReferenceColumnId() {
		return referenceColumnId;
	}

	/**
	 * @param referenceCollectionIdentity the referenceCollectionIdentity to set
	 */
	public void setReferenceCollectionIdentity(String referenceCollectionIdentity) {
		this.referenceCollectionIdentity = referenceCollectionIdentity;
	}

	/**
	 * @param referenceColumnId the referenceColumnId to set
	 */
	public void setReferenceColumnId(Integer referenceColumnId) {
		this.referenceColumnId = referenceColumnId;
	}

	public String getColumnType() {
		return columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	/**
	 * @return the actualName
	 */
	public String getActualName() {
		return actualName;
	}

	/**
	 * @param actualName the actualName to set
	 */
	public void setActualName(String actualName) {
		this.actualName = actualName;
	}
	
}
