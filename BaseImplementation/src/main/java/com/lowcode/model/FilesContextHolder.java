package com.lowcode.model;

import java.util.HashMap;

import org.springframework.web.multipart.MultipartFile;

public class FilesContextHolder {

	private static final ThreadLocal<HashMap<String,MultipartFile>> files = new ThreadLocal<HashMap<String,MultipartFile>>();

    
	public static HashMap<String,MultipartFile> getFiles() {
		return files.get();
	}
	
	public static void setFiles(HashMap<String,MultipartFile> file) {
		files.set(file);
	}


}
