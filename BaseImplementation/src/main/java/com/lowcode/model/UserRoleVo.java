package com.lowcode.model;

import java.util.List;

public class UserRoleVo {
    private String roleName;
    private String description;
    private Integer roleForId;
    private String roleForName;
    private String identity;
    private List<ApplicationMappingVo> appMapping;
    private List<UserRoleMappingVo> userRoleMapping;
    private Integer roleId;
    
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getRoleForId() {
		return roleForId;
	}
	public void setRoleForId(Integer roleForId) {
		this.roleForId = roleForId;
	}
	public String getRoleForName() {
		return roleForName;
	}
	public void setRoleForName(String roleForName) {
		this.roleForName = roleForName;
	}
	public String getIdentity() {
		return identity;
	}
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	public List<ApplicationMappingVo> getAppMapping() {
		return appMapping;
	}
	public void setAppMapping(List<ApplicationMappingVo> appMapping) {
		this.appMapping = appMapping;
	}
	public List<UserRoleMappingVo> getUserRoleMapping() {
		return userRoleMapping;
	}
	public void setUserRoleMapping(List<UserRoleMappingVo> userRoleMapping) {
		this.userRoleMapping = userRoleMapping;
	}
}
