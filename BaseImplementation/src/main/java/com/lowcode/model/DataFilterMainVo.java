package com.lowcode.model;

import java.util.List;

public class DataFilterMainVo {
	
	private List<SubGroupVo> subGroups;
	private Integer subGroupType;
	private String subGroupTypeName;
	private String identity;
	private Integer dataFilterMainId;
	/**
	 * @return the subGroups
	 */
	public List<SubGroupVo> getSubGroups() {
		return subGroups;
	}
	/**
	 * @param subGroups the subGroups to set
	 */
	public void setSubGroups(List<SubGroupVo> subGroups) {
		this.subGroups = subGroups;
	}
	/**
	 * @return the subGroupType
	 */
	public Integer getSubGroupType() {
		return subGroupType;
	}
	/**
	 * @param subGroupType the subGroupType to set
	 */
	public void setSubGroupType(Integer subGroupType) {
		this.subGroupType = subGroupType;
	}
	/**
	 * @return the subGroupTypeName
	 */
	public String getSubGroupTypeName() {
		return subGroupTypeName;
	}
	/**
	 * @param subGroupTypeName the subGroupTypeName to set
	 */
	public void setSubGroupTypeName(String subGroupTypeName) {
		this.subGroupTypeName = subGroupTypeName;
	}
	/**
	 * @return the identity
	 */
	public String getIdentity() {
		return identity;
	}
	/**
	 * @param identity the identity to set
	 */
	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public Integer getDataFilterMainId() {
		return dataFilterMainId;
	}

	public void setDataFilterMainId(Integer dataFilterMainId) {
		this.dataFilterMainId = dataFilterMainId;
	}

}
