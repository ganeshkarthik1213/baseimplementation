package com.lowcode.model;

public class UserRoleMappingVo {

	private String identity;			 
	private String userIdentity; 			
	private String userRoleIdentity; 		
	private boolean isDeleted;
	
	public String getIdentity() {
		return identity;
	}
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	public String getUserIdentity() {
		return userIdentity;
	}
	public void setUserIdentity(String userIdentity) {
		this.userIdentity = userIdentity;
	}
	public String getUserRoleIdentity() {
		return userRoleIdentity;
	}
	public void setUserRoleIdentity(String userRoleIdentity) {
		this.userRoleIdentity = userRoleIdentity;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	
	
}
