package com.lowcode.CommonEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "Page1", catalog = "$schemaName")
public class Page1 implements Serializable {
   	@Id
   	@GeneratedValue(strategy = GenerationType.IDENTITY)
  	private Integer primaryKey;
	private String _id;
	private Boolean is_Deleted;
        private String TextBox_1_91394;
        private Boolean Checkbox_2_91395;
        private Integer Radio_3_91396;
}
