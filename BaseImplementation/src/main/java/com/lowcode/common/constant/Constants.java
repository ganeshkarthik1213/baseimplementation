package com.lowcode.common.constant;

public class Constants {

	public static final String DEFAULT_TIMEZONE = "EST";

	public static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";

	public static final String DOT_REGEX = "\\.";

	public static final String DEFAULT_DATE_FORMAT = "MM/dd/yyyy";

	public static final String DEFAULT_FULL_DATE_FORMAT = "MM/dd/yyyy HH:mm";
	
	public static final String TIME_AND_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm";

	public static final Integer NOOFDAYSPERYEAR = 365;

	public static final String DATE_FORMAT_WITH_SEC = "MM/dd/yyyy HH:mm:ss";

	public static final String HHMM = "hh:mm";

	public static final String DATE_FORMAT_WITH_AM_PM = "MM/dd/yyyy hh:mm a";

	public static final String ENTITY_IDENTITY = "[_IDENTITY]";

	public static final String ENTITY_UNIQUE_VALUE = "[ENTITY_UNIQUE_VALUE]";

	public static final String CLIENT_FIELD_VALUE = "[ENTITY_UNIQUE_CLIENT_VALUE]";

	public static final String ENTITY_VALUE = "?";

	public static final String ENTITY_ISDELETED_VALUE = "[IS_DELETED]";

	public static final String CLIENT_IN_COOKIE = "client";

	public static final String USER_IN_COOKIE = "user";

	public static final String DOT = ".";

	public static final String MINUS_ONE = "-1";

	public static final String FILTER_TYPE_INTEGER = "Integer";
	public static final String FILTER_TYPE_DOUBLE = "Double";
	public static final String FILTER_TYPE_BOOLEAN = "Boolean";
	public static final String FILTER_TYPE_DATE = "Date";
	public static final String FILTER_TYPE_TEXT = "String";
	public static final String FILTER_TYPE_LONG = "Long";
	public static final String FILTER_TYPE_CURRENCY = "Currency";
	public static final String EQUAL = "Equal";
	public static final String GTE = "Ge";
	public static final String LTE = "Le";
	public static final String LT = "Lt";
	public static final String GT = "Gt";
	public static final String LIKE = "Like";
	public static final String IN = "IN";
	public static final String NOT_IN = "NOT_IN";
	public static final String BETWEEN = "BW";
	public static final String ORDERBY = "orderBy";
	public static final String ROUNDOFF = "ROUNDOFF";
	public static final String FILTER = "FILTER";
	public static final String SORTING = "SORTING";
	public static final String EQUALS = "equals";
	
	public static final String DROPDOWN_FIELD = "Dropdown";
	public static final String AUTOCOMPLETE_FIELD = "Autocomplete";
	public static final String MULTISELECT_FIELD = "Multiselect";

	public static final String IDENTITY = "Identity";
	public static final String IDENTITY_CONSTANT = "identity";
	public static final String UNDERSCORE = "_";
	public static final String DELETED = "deleted";
	public static final String ACTIVE = "isActive";

	public static final String CREATED_BY = "  Created by  ";
	public static final String MODIFIED_BY = "  Modified by ";
	public static final String CREATED_ON = " Created on";
	public static final String MODIFIED_ON = "  Modified on  ";

	public static final String SEMI_COLON = ";";
	
	public static final String FIELDS_SEPARATOR = ",";
	
	public static final String PERCENTAGE = "%";

	public static final char SINGLE_DOUBLE_QUOTE = '"';

	public static final String MANDATORY_FUNCTIONALITY_NAME = "MANDATORY";
	
	public static final String SYSTEM_GENERATED = "System Generated";
	public static final String LINK_MASTER_DATA = "Link Master Data";
	public static final String REFERENCE_OPTION = "Reference";
	public static final String CUSTOM_OPTION = "Custom Options";
	public static final String EXTERNAL_API = "External API Data";
	
	public static final String EXTERNAL_API_OPTION = "External API Data";
	
	// workflow graph constants
	
	public static final String EXTERNAL_SERVICE = "externalService";
	public static final String ANOTHER_FORM = "anotherForm";
	public static final String SEND_MAIL = "sendMail";
	public static final String CUSTOM_CALL = "customCall";
	public static final String APPROVAL = "approval";
	public static final String REDIRECT = "redirect";
	public static final String SAVE_OR_UPDATE = "saveOrUpdate";
	
	//workflow constants
	
	public static final String NEW_PAGE_ID = "newPageId";
	public static final String ID = "_id";
	public static final String REDIRECT_URL = "redirectUrl";
	public static final String URL_OPEN_TYPE = "openType";
	public static final String WEBSITE = "Website";
	public static final String OPEN_TYPE = "openType";
	public static final String SAME = "same";
	public static final String NEW = "new";
	public static final String INNER_PAGE = "Inner Page";
	public static final String SAME_WINDOW = "Same Window";
	public static final String NEW_WINDOW = "New Window";
	public static final String URL_TYPE = "urlType";
	public static final String Text = "Text";
	public static final String User_Defined = "User Defined";
	public static final String User_Params = "User Params";
	
	
	// Table Names
	
	public static final String USER_ROLE= "USER_ROLES";
	public static final String APPLICATION_MAPPING= "APPLICATION_MAPPING";
	public static final String FUNCTIONALITY_MAPPING= "FUNCTIONALITY_MAPPING";
	public static final String STATIC_PAGES= "STATIC_PAGES";
	public static final String FUNCTIONALITY_PAGE_MAPPING="FUNCTIONALITY_PAGE_MAPPING";
	public static final String FUNCTIONALITY_STATIC_PAGES_MAPPING = "FUNCTIONALITY_STATIC_PAGES_MAPPING";
	
	
	// user and user Role constants number
	
	public static final int USER_NUM = 1;
	public static final int USER_ROLE_NUM = 2;
	
	// App and Dev User Type
	public static final int APP_USER_TYPE = 1;
	public static final int DEV_USER_TYPE = 2;
	public static final int SUPER_APP_USER_TYPE = 4;
	
	public static final String BUTTON_FIELD_TYPE_NAME= "Button";
	public static final String ICON_FIELD_TYPE_NAME= "Icon";
	public static final String FILE_UPLOAD= "File Upload";


	
	public static final String GRID = "Grid";
	public static final String RADIO = "Radio";
	public static final String SUBFORM = "Sub Form";
	public static final String  HTML_RENDER= "Html Render";
	public static final String  MAPPING= "Mapping";

	
	// Added for Import Application...
	public static final Long GRIDFS_FILE_CHUNK_SIZE = 1024000L;
	public static final String FILE_NAME = "fileName";
	public static final String FILE_TYPE = "fileType";
	public static final String FILE_SIZE = "fileSize";
	public static final String UPLOADED_DATE = "uploadedDate";
	public static final String SHEET_NAMES = "sheetNames";
	public static final String NO_OF_SHEETS = "noOFSheets";
	public static final String DOCUMENT_ID = "documentId";
	public static final String ORIGINAL="ORIGINAL_";
	public static final String ODS = "ods";
	public static final String IMPORT_IMAGE_DEF_NAME = "import_export";
	
	public static final String IMPORT_APP_COLLECTION_NAME="APPLICATION_FILE_IMPORT";
	
	public static final String SAVE_DATA_WORKFLOW = "Save Data";
	public static final String REDIRECT_WORKFLOW = "Redirect Url";

	public static final String GET_METHOD = "GET";
	public static final String POST_METHOD = "POST";

	public static final String DATE_TYPE = "Date";
	public static final String DATE_TIME_TYPE = "Date And Time";
	public static final String TIME_TYPE = "Time";
	public static final String FILE_DATA_TYPE = "File Upload";

	public static final String CREATED_DATE = "Created Date";
	public static final String MODIFIED_DATE = "Modified Date";
	public static final String DATE_FORMAT = "yyyy-MM-dd";

	//	Icon for grid filter from import Application ...
	public static final String FILTER_ICON_NAME = "FILTER_1";

	public static final String TEXT_LABEL = "Text Label";
	public static final String Action = "Actions";
	
	public static final String DROPDOWN_TYPE = "Dropdown";
	public static final String AUTOCOMPLETE_TYPE = "Autocomplete";
	public static final String MULTISELECT_TYPE = "Multiselect";
	public static final String PHONE_NUMBER_TYPE = "Phone Number";
	public static final String CHECKBOX_TYPE = "Checkbox";
	public static final String CURRENCY_TYPE = "Currency";
	public static final String TEXTLABEL_TYPE_CENTER = "center";
	public static final String TEXTLABEL_TYPE_LEFT = "left";
	public static final String TEXTLABEL_TYPE_RIGHT = "right";
	public static final String TEXTLABEL_TYPE_JUSTIFY = "justify";
	public static final String TEXTLABEL_TYPE_NORMAL = "normal";
	public static final String DESKTOP = "Desktop";
	public static final String TAB = "Tab";
	public static final String MOBILE = "Mobile";
	public static final String Email_Type = "Email";
	public static final String Text_Box = "Text Box";
	public static final String Text_Area = "Text Area";
	public static final String String = "String";
	public static final String Number = "Number";

	public static final String VERSIONS = "Version";
	
	public static final String SUB_FORM = "Sub Form";
	public static final String Container = "Container";
	public static final String MAP = "Map";

	public static final String MONGO = "mongo";
	public static final String MYSQL = "mysql";
	public static final String SQL_SERVER = "sqlServer";
	
	public static final String EXISTING_TABLE = "EXISTING TABLE";
	public static final String SUPER_APP_ADMIN = "Super App Admin";
	public static final String USER_ROLES = "USER_ROLE";
	public static final String SPECIAL_ENTITY = "Special Entity";
	public static final String ROLE= "USER_ROLE";
	public static final String MONTH_AND_YEAR_TYPE = "Month And Year";
	public static final String DCT_THEME_TYPE = "5";
	public static final String REFERENCE_FIELDS = "Reference Fields";
	public static final String STATIC = "Static";
	
	public static final String SET_DYANMIC_MONTH = "Set Dynamic Month";
	public static final String AFTER = "After";
	public static final String BEFORE = "Before";
	
}
