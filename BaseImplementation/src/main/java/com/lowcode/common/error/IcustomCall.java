package com.lowcode.common.error;

import com.lowcode.exceptions.LowCodeException;

public interface IcustomCall {

	String saveUserCheck(String saveUserDetailsVo,String loggedInUser) throws LowCodeException;
	
	String updateUserCheck(String userDetailsVo, String loggedInUser);

	boolean getUserDetailsCheck(String userName);

}
