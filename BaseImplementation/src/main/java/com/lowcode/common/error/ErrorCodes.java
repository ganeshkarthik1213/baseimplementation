package com.lowcode.common.error;


public interface ErrorCodes {
	ErrorId INTERNAL_ERROR = new ErrorId("E1000", "Something went wrong. Please try again...");
	ErrorId INVALID_USER = new ErrorId("E1002", "Please login to continue..");
	ErrorId PAGE_TYPE_REQUIRED = new ErrorId("E11006", "Page type required");
	ErrorId INVALID_PASSWORD = new ErrorId("E11006", "Password cannot be null");
	ErrorId ERROR_MENU_CREATION = new ErrorId("E11006", "Menu creation failed");
	ErrorId ERROR_GET_RECORDS = new ErrorId("E11007", "Records are not found");
	ErrorId ERROR_RECORDS_COUNT = new ErrorId("E11008", "No data count");
	ErrorId INVALID_MODULE_ID =  new ErrorId("E11009","Invalid ModuleId");
	ErrorId IDENTITY_DATA_REQUIRED =  new ErrorId("E11010","Required Fields is Missing");
	ErrorId FILL_ALL_MANDATORY_COLUMNS = new ErrorId("E11011","fields are not filled");
	ErrorId MANDATORY_COLUMNS_NOT_AVAILABLE = new ErrorId("E11012","Mandatory Columns are not exsit");
	ErrorId PAGE_ID_INAVALID = new ErrorId("E11013","Page Id is Invalid");
	ErrorId REFERENCEIDENTITY_INAVALID = new ErrorId("E11014","Reference Identity is Invalid");
	ErrorId INVALID_IDENTITY = new ErrorId("E11015","Identity is null");
	ErrorId INVALID_CONDITION = new ErrorId("E11006", "Condition type is invalid");
	ErrorId INVALID_ACTION = new ErrorId("E11006", "Action type is invalid");
	ErrorId APPLICATION_ID_INAVALID = new ErrorId("E110016", "Application Id is Invalid");
	ErrorId ERROR_WHILE_UPLOAD_FILE = new ErrorId("E110017", "Error while upload file");
}
