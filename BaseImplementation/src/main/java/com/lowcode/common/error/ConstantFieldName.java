package com.lowcode.common.error;

public interface ConstantFieldName {
	public static final String MONGO_DB_NAME = "mydb";
	public static final String MONGO_IMAGE_DB_NAME = "imageDb";
	public static final String MONGO_DOCUMENT_DB_NAME = "documentDb";
	public static final String MONGO_IMAGE_TABLE_NAME = "photo";
	public static final String MONGO_DOCUMENT_TABLE_NAME = "customerDocuments";
	public static final String MONGO_SEQUENCE_COLLECTION_NAME = "SEQUENCECOUNTER";
	public static final String MONGO_SEQUENCE_ID_NAME = "sequenceid";
}
