package com.lowcode.common.filter;

import java.util.List;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.lowcode.common.constant.Constants;



public interface DataFilter {
	
	default Predicate getPredicate(DataFilterRegistry dataFilterRegisty, Root<?> root, FilterNameEnum filterNameEnum,
			List<?> args, Object key) {
		String[] referenceNames = dataFilterRegisty.getDataFilters(key).get(filterNameEnum).split(Constants.DOT_REGEX);
		Path<String> expression = root.get(referenceNames[0]);
		if (referenceNames.length > 1) {
			expression = expression.get(referenceNames[1]);
		}
		Predicate predicate = expression.in(args);
		if (isValidList(args)) {
			return predicate;
		}
		return null;
	}

	Predicate getFilter(Root<?> root, Object key);
	public static boolean isValidList(List<?> input) {
		if (input != null && input.size() > 0) {
			return true;
		}
		return false;
	}
}
