package com.lowcode.common.filter;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value = "transactionManager")
@Qualifier("adminFilter")
public class AdminFilter implements DataFilter {

	@Autowired
	private DataFilterRegistry dataFilterRegisty;

	@Override
	public Predicate getFilter(Root<?> root, Object key) {
		List<String> ids = new ArrayList<>();
		return getPredicate(dataFilterRegisty, root, FilterNameEnum.ADMIN_FILTER, ids, key);
	}

}
