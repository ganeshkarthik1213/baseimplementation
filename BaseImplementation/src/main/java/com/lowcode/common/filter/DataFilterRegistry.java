package com.lowcode.common.filter;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class DataFilterRegistry {
	private static Map<Object, Map<FilterNameEnum, String>> filterRegisty = new HashMap<Object, Map<FilterNameEnum, String>>();

	public Map<FilterNameEnum, String> getDataFilters(Object key) {
		
		return filterRegisty.get(key);
	}

	/**
	 * 
	 * @param key             current object
	 * @param filterNameEnum  which filter we want
	 * @param referanceColumn reference column name for current entity
	 */
	public void add(Object key, FilterNameEnum filterNameEnum, String referenceColumn) {
		if (key == null) {
			return;
		}

		if (filterRegisty.get(key) == null) {
			filterRegisty.put(key, new HashMap<FilterNameEnum, String>());
		}

		filterRegisty.get(key).put(filterNameEnum, referenceColumn);
	}

	public void remove(Object key) {
		if (key == null)
			return;
		if (filterRegisty.get(key) == null)
			return;
		filterRegisty.remove(key);
	}
}
