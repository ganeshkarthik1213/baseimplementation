package com.lowcode.common.filter;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lowcode.base.dao.BaseDAOImpl;
import com.lowcode.exceptions.LowCodeUnauthorizedException;
import com.lowcode.model.User;
import com.lowcode.userManager.IuserManagerservice;


@Service 
@Transactional(value="transactionManager") 
@Qualifier("businessFilter") 
public class BusinessDataFilter extends BaseDAOImpl implements DataFilter{

	@Autowired 
	private DataFilterRegistry dataFilterRegisty; 
	
	@Autowired
	private IuserManagerservice userManager;
	
	public User getLoggedInUser() throws LowCodeUnauthorizedException {
		return userManager.getLoggedInUser();
		
	}

	 

	@Override 
	public Predicate getFilter(Root<?> root, Object key) { 
       List<String> ids = new ArrayList<>();
       User appUser = null;
       try {
		 appUser = userManager.getLoggedInUser();
	    } catch (LowCodeUnauthorizedException e) {
		  e.printStackTrace();
	   }
       if(appUser !=null && appUser.getBuisnessId() !=null) {
    	   ids.add(appUser.getBuisnessId().getIdentity());
    	   return getPredicate(dataFilterRegisty, root, FilterNameEnum.BUISNESS_FILTER, ids, key);
       }
	   return null;

	} 

}
