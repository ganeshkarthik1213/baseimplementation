package com.lowcode.common.filter;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class DataFilterFactory {
	
	private Map<FilterNameEnum, DataFilter> filterMap = null;

	@Autowired
	@Qualifier("adminFilter")
	private DataFilter adminFilter;
	
	@Autowired 
	@Qualifier("businessFilter") 
	private DataFilter businessFilter;

	@PostConstruct
	private void buildMap() {
		filterMap = new HashMap<FilterNameEnum, DataFilter>();
		filterMap.put(FilterNameEnum.ADMIN_FILTER, adminFilter);
		filterMap.put(FilterNameEnum.BUISNESS_FILTER, businessFilter); 
	}

	public DataFilter getFilterByName(FilterNameEnum filterName) {
		return filterMap.get(filterName);
	}

}